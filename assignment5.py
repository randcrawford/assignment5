#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

valid_ids = {1,2,3}

@app.route('/book/JSON/')
def bookJSON():
    return render_template('json.html', books = books)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
	if request.method == 'POST':
		name = request.form['name']
		new_id = 0
		for i in range(1,10000):
			if i not in valid_ids:
				new_id = i
				break
		books.append({'title': name, 'id': str(new_id)})
		valid_ids.add(new_id)
		return redirect(url_for('showBook'))
	return render_template('newBook.html');

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
	if request.method == 'POST':
		name = request.form['name']
		for b in books:
			print(b)
			if int(b['id']) == book_id:
				b['title'] = name
				return redirect(url_for('showBook'))
	return render_template('editBook.html', book_id = book_id)

@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
	if request.method == 'POST':
		for b in books:
			if int(b['id']) == book_id:
				books.remove(b)
				valid_ids.remove(book_id)
				return redirect(url_for('showBook'))
	return render_template('deleteBook.html', book_id = book_id)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 8000)
	

